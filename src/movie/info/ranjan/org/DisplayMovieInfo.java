package movie.info.ranjan.org;


import java.util.ArrayList;
import java.util.function.Consumer;

public class DisplayMovieInfo {

	public static void main(String[] args) {
		ArrayList<Movie> l=new ArrayList<>();
		populate(l);
		
		Consumer<Movie> c=m->
		{
			System.out.println("Movie Name :"+m.name);
			System.out.println("Movie Hero :"+m.hero);
			System.out.println("Movie Heroin :"+m.heroin);
			System.out.println("******************");
		};
		for(Movie m : l){
			c.accept(m);
		}
	}
	public static void populate(ArrayList<Movie> l){
		l.add(new Movie("Bahubali", "Prabhas", "Anuska"));
		l.add(new Movie("Rayees", "Sharukh", "Sunny"));
		l.add(new Movie("Dhangal", "Amir", "Ritu"));
		l.add(new Movie("Sultan", "Salman", "Anuska"));
		
	}

}
