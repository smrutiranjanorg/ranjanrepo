package movie.info.ranjan.org;
import java.util.ArrayList;
import java.util.function.Consumer;

public class DisplayMovieStatus {

	public static void main(String[] args) {
		ArrayList<Movie> l=new ArrayList<>();
		populate(l);
		
		Consumer<Movie> c1=m->System.out.println("Movie Name :"+m.name+" is ready to release");
		Consumer<Movie> c2=m->System.out.println("Movie Name :"+m.name+" is "+m.result);
		Consumer<Movie> c3=m->System.out.println("Movie Name :"+m.name+" information Storing in database");
		
		Consumer<Movie> chainedConsumer=c1.andThen(c2).andThen(c3);
		
		for(Movie m : l){
			chainedConsumer.accept(m);
			System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
		}

	}
	public static void populate(ArrayList<Movie> l){
		l.add(new Movie("Bahubali", "SuperHit"));
		l.add(new Movie("Rayees", "Flop"));
		l.add(new Movie("Dhangal", "Superhit"));
		l.add(new Movie("Sultan", "Hit"));
		
	}

}
